FROM infrastructuregr/terraform-kubectl

ENV AWSCLI_VER 1.19.76
ENV HELM_VERSION v3.5.4
ENV KUBECTL_VER v1.21.1
ENV HELM_S3_VERSION 0.10.0

RUN apk add --update --no-cache git py-pip bash \
    && pip install awscli==$AWSCLI_VER \
    && curl https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz | tar xvzf - --strip-components=1 -C /usr/bin \
    && rm -f /var/cache/apk/*

RUN aws --version

ADD https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VER/bin/linux/amd64/kubectl /usr/bin/kubectl
RUN chmod +x /usr/bin/kubectl

ADD https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator /usr/bin/aws-iam-authenticator
RUN chmod +x /usr/bin/aws-iam-authenticator

RUN helm plugin install https://github.com/hypnoglow/helm-s3.git --version $HELM_S3_VERSION

ENTRYPOINT ["/bin/bash"]