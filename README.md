# Configuração necessária no CI/CD dos repositórios que utilizarão este container

Setar as variáveis de ambiente:

| NAME                  	| TYPE     	| VALUE                   	|
|-----------------------	|----------	|-------------------------	|
| AWS_ACCESS_KEY_ID     	| Variable 	| <aws_access_key_id>     	|
| AWS_SECRET_ACCESS_KEY 	| Variable 	| <aws_secret_access_key> 	|
| AWS_DEFAULT_REGION    	| Varialbe 	| <aws_default_region>    	|
| K8S_CONFIG            	| File     	| <~/.kube/config>        	|