image: docker:19.03.15

services:
  - docker:19.03.15-dind

# executa o pipeline apenas em casos específicos
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"
    - if: $CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "stable"
    - if: $CI_COMMIT_TAG =~ /^v(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)$/
    

variables:
  # usar "tcp://docker:2375/" para execução do pipeline nos "shared runners" do gitlab
  # ou "tcp://localhost:2375/" para execução no cluster kubernetes da IHM (desativar shared runners na configuração do repositório no gitlab)
  DOCKER_HOST: tcp://docker:2375/
  # When using dind, it's wise to use the overlayfs driver for improved performance.
  DOCKER_DRIVER: overlay2
  # This will instruct Docker not to start over TLS.
  DOCKER_TLS_CERTDIR: ""
  # mudar para true em caso de necessidade de debug
  CI_DEBUG_TRACE: "false"

######################################## PROJECT VARIABLES ########################################

  CHART_NAME: k8s-deployer # CHART_NAME deve ter o mesmo nome da pasta em ./kubernetes/CHART_NAME

######################################## RULES, JOBS & YAML ANCHORS ########################################

# executa apenas com commit na "master"
.if-master: &if-master
  if: '$CI_COMMIT_BRANCH == "master"'

# executa apenas com versão tageada no padrão semver (e.g. "v8.4.12") na branch "stable"
.if-tagged-release: &if-tagged-release
  # padrão semver limitado
  # iniciando com 'v', apenas com major, minor e patch, todos sem leading zeros (e.g. "v2.1.8")
  if: '$CI_COMMIT_TAG =~ /^v(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)$/'

# executa com merge request da "master" na "stable"
.if-merge-request-on-master-or-stable: &if-merge-request-on-master-or-stable
  if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "stable"'

.only-default:
  rules:
    - <<: *if-master
    - <<: *if-tagged-release
    - <<: *if-merge-request-on-master-or-stable
      when: on_success
    - when: never

.only-master:
  rules: 
    - <<: *if-master
      when: on_success
    - when: never


.only-tagged-release:
  rules: 
    - <<: *if-tagged-release
      when: on_success
    - when: never

# carrega imagem dos estágios de build
.load-image:
  before_script:
    # carrega imagem do estágio anterior
    - docker load -i $CI_PROJECT_DIR/$CI_PROJECT_NAME:current.tar
    # realiza login no container registry privado
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# empurra imagens para container registry privado. Exige $TAGS (podem receber múltiplas separadas por vírgulas)
.push-image:
  script:
    # adiciona tags na imagem e faz uploads
    - >
      for tag in $(echo $TAGS | sed 's/,/ /g');
      do
        echo "pushing image with tag $tag";
        docker tag $CI_REGISTRY_IMAGE:current $CI_REGISTRY_IMAGE:$tag;
        docker push $CI_REGISTRY_IMAGE:$tag;
      done

#  configura repositório privado de charts.
.helm-repo-config:
  image: registry.gitlab.com/ihm-public/k8s-deployer:latest
  # adiciona repositório de charts privados
  before_script:
    - helm repo add ihmcharts s3://br-com-ihm-charts/charts


######################################## STAGES ########################################

stages:
  - check
  - build
  - test
  - push-image
  - package-chart
  - deploy
  - verify
  - logs

######################################## STAGE:CHECK ########################################

check-tag-branch:
  stage: check
  image: alpine/git:latest
  extends: 
    - .only-tagged-release
  script:
    # TODO: corrigir erro
    - echo 'dummy'
    # - 'HEADS=$(git ls-remote -q --heads | grep $CI_COMMIT_SHA)'
    # - echo $HEADS
    # - 'expr match "$HEADS" ".*stable$"'


######################################## STAGE:BUILD ########################################

build:backend:
  stage: build
  extends: 
    - .only-default
  script:
    # tenta baixar imagem do último build para usar cache
    - docker pull $CI_REGISTRY_IMAGE:current || true
    # compila imagem tentando usar cache
    - docker build --cache-from $CI_REGISTRY_IMAGE:current --tag $CI_REGISTRY_IMAGE:current --file $CI_PROJECT_DIR/Dockerfile $CI_PROJECT_DIR
    # salva imagem em um arquivo .tar para ser utilizado por outros estágios
    - docker save $CI_REGISTRY_IMAGE:current > $CI_PROJECT_DIR/$CI_PROJECT_NAME:current.tar
  artifacts:
    # expõe imagem em formato ".tar" como artefato para ser acessado por outros estágios
    # expira em 2h, tempo suficiente para outros estágios executarem, evita consumir limite de espaço do repositório a longo prazo
    expire_in: 2 hour
    paths:
      - $CI_PROJECT_DIR/$CI_PROJECT_NAME:current.tar
  

######################################## STAGE:TEST ########################################

# test-e2e:
#   stage: test
#   dependencies:
#     - "build:backend"
#   extends:
#     - .load-image
#     - .only-default
#   script:
#     - docker image ls
#     # docker-compose install
#     # docker-compose up
#     # run tests
  

######################################## STAGE:PUSH-IMAGE ########################################

# Release current image (branch master)
push-image:current:
  stage: push-image
  dependencies:
    - "build:backend" # to access image shared by artifact
  extends:
    - .load-image
    - .push-image
    - .only-master
  variables:
    TAGS: "current"

# Release tagged version image (branch stable)
push-image:latest:
  stage: push-image
  dependencies:
    - build:backend # to access image shared by artifact
  extends:
    - .load-image
    - .push-image
    - .only-tagged-release
  variables:
    TAGS: latest,$CI_COMMIT_TAG
  